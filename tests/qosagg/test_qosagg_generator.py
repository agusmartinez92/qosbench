import textwrap
from io import StringIO

from qosbench.qosagg.qosagg_generator import QosAggGenerator, QosAggGeneratorAscendingValues, QosAggGeneratorConfig


def test_qosagg_generator(output: StringIO) -> None:
    config = QosAggGeneratorConfig(2, 2, 2, 2, 2, 2, 1)
    QosAggGenerator(config, output).generate()

    assert output.getvalue() == textwrap.dedent("""\
    workflow {
    \tgraph: ((A B | C D) -> (E F + G H))^2;
    
    \taggregation attr_0: sum, max;
    
    \tprovision a0 for A: attr_0 = 3.5011;
    \tprovision a1 for A: attr_0 = 8.7576;
    \tprovision b0 for B: attr_0 = 3.6907;
    \tprovision b1 for B: attr_0 = 1.7277;
    \tprovision c0 for C: attr_0 = 7.5902;
    \tprovision c1 for C: attr_0 = 7.6296;
    \tprovision d0 for D: attr_0 = 6.3316;
    \tprovision d1 for D: attr_0 = 3.9929;
    \tprovision e0 for E: attr_0 = 5.3869;
    \tprovision e1 for E: attr_0 = 7.5739;
    \tprovision f0 for F: attr_0 = 7.1705;
    \tprovision f1 for F: attr_0 = 0.5822;
    \tprovision g0 for G: attr_0 = 5.7660;
    \tprovision g1 for G: attr_0 = 9.8504;
    \tprovision h0 for H: attr_0 = 8.7430;
    \tprovision h1 for H: attr_0 = 0.6672;
    };
    """)  # noqa: W293

def test_qosagg_generator_ascending_values(output: StringIO) -> None:
    config = QosAggGeneratorConfig(3, 3, 3, 3, 2, 3, 2)
    QosAggGeneratorAscendingValues(config, output).generate()

    assert output.getvalue() == textwrap.dedent("""\
    workflow {
    \tgraph: ((A B C | D E F | G H I) -> (J K L + M N O + P Q R))^2;

    \taggregation attr_0: sum, max;
    \taggregation attr_1: sum, max;

    \tprovision a0 for A: attr_0 = 1.0059, attr_1 = 9.8829;
    \tprovision a1 for A: attr_0 = 10.0855, attr_1 = 19.3507;
    \tprovision a2 for A: attr_0 = 23.6921, attr_1 = 25.3037;
    \tprovision b0 for B: attr_0 = 6.0714, attr_1 = 8.5434;
    \tprovision b1 for B: attr_0 = 12.6728, attr_1 = 16.5314;
    \tprovision b2 for B: attr_0 = 29.5426, attr_1 = 25.3214;
    \tprovision c0 for C: attr_0 = 5.2089, attr_1 = 8.5819;
    \tprovision c1 for C: attr_0 = 11.3526, attr_1 = 19.6919;
    \tprovision c2 for C: attr_0 = 22.2531, attr_1 = 29.8244;
    \tprovision d0 for D: attr_0 = 7.0020, attr_1 = 1.9639;
    \tprovision d1 for D: attr_0 = 14.6472, attr_1 = 19.0145;
    \tprovision d2 for D: attr_0 = 26.2560, attr_1 = 28.5171;
    \tprovision e0 for E: attr_0 = 0.5666, attr_1 = 5.2029;
    \tprovision e1 for E: attr_0 = 13.1786, attr_1 = 16.4706;
    \tprovision e2 for E: attr_0 = 24.3337, attr_1 = 26.3676;
    \tprovision f0 for F: attr_0 = 0.4691, attr_1 = 0.4132;
    \tprovision f1 for F: attr_0 = 17.0159, attr_1 = 19.4259;
    \tprovision f2 for F: attr_0 = 27.6483, attr_1 = 29.9654;
    \tprovision g0 for G: attr_0 = 6.7665, attr_1 = 6.6630;
    \tprovision g1 for G: attr_0 = 12.2577, attr_1 = 15.2698;
    \tprovision g2 for G: attr_0 = 22.2042, attr_1 = 29.0883;
    \tprovision h0 for H: attr_0 = 1.1447, attr_1 = 0.3674;
    \tprovision h1 for H: attr_0 = 14.3052, attr_1 = 17.2739;
    \tprovision h2 for H: attr_0 = 28.0178, attr_1 = 23.5773;
    \tprovision i0 for I: attr_0 = 3.7148, attr_1 = 9.5910;
    \tprovision i1 for I: attr_0 = 12.7032, attr_1 = 14.6386;
    \tprovision i2 for I: attr_0 = 29.9308, attr_1 = 29.8006;
    \tprovision j0 for J: attr_0 = 6.7484, attr_1 = 4.9769;
    \tprovision j1 for J: attr_0 = 19.2735, attr_1 = 12.5119;
    \tprovision j2 for J: attr_0 = 26.0158, attr_1 = 24.0840;
    \tprovision k0 for K: attr_0 = 6.4288, attr_1 = 3.5596;
    \tprovision k1 for K: attr_0 = 17.2775, attr_1 = 11.4427;
    \tprovision k2 for K: attr_0 = 20.9042, attr_1 = 29.7810;
    \tprovision l0 for L: attr_0 = 1.3255, attr_1 = 0.8100;
    \tprovision l1 for L: attr_0 = 19.2191, attr_1 = 10.6196;
    \tprovision l2 for L: attr_0 = 28.4590, attr_1 = 25.7362;
    \tprovision m0 for M: attr_0 = 0.0253, attr_1 = 1.8376;
    \tprovision m1 for M: attr_0 = 17.0654, attr_1 = 18.9765;
    \tprovision m2 for M: attr_0 = 24.6634, attr_1 = 20.1667;
    \tprovision n0 for N: attr_0 = 9.8094, attr_1 = 6.4568;
    \tprovision n1 for N: attr_0 = 10.0554, attr_1 = 10.1847;
    \tprovision n2 for N: attr_0 = 24.9519, attr_1 = 27.0506;
    \tprovision o0 for O: attr_0 = 8.4177, attr_1 = 3.3447;
    \tprovision o1 for O: attr_0 = 19.1557, attr_1 = 19.8984;
    \tprovision o2 for O: attr_0 = 26.2802, attr_1 = 20.0458;
    \tprovision p0 for P: attr_0 = 3.5042, attr_1 = 0.5944;
    \tprovision p1 for P: attr_0 = 10.5732, attr_1 = 19.2730;
    \tprovision p2 for P: attr_0 = 20.5008, attr_1 = 28.1394;
    \tprovision q0 for Q: attr_0 = 2.9794, attr_1 = 2.3681;
    \tprovision q1 for Q: attr_0 = 18.5914, attr_1 = 16.6140;
    \tprovision q2 for Q: attr_0 = 25.7879, attr_1 = 21.0451;
    \tprovision r0 for R: attr_0 = 7.0069, attr_1 = 0.6009;
    \tprovision r1 for R: attr_0 = 11.0862, attr_1 = 19.1287;
    \tprovision r2 for R: attr_0 = 28.5290, attr_1 = 23.4722;
    };
    """)  # noqa: W293