import csv
import math
from typing import TextIO, Tuple, Iterable, Dict, Sequence


def read_and_filter(input_file: TextIO, filters: Dict[str, int], x_column: str) -> Tuple[Iterable[Dict], Sequence[str]]:
    data, header = read_data(input_file)
    if x_column not in header:
        raise ValueError(f"{x_column} is not contained in the data set")
    return filter(lambda row: matches_filters(row, filters), data), header


def read_data(input_file: TextIO) -> Tuple[Iterable[Dict], Sequence[str]]:
    reader = csv.DictReader(input_file)
    assert reader.fieldnames is not None
    headers: Sequence[str] = reader.fieldnames

    def read() -> Iterable[Dict]:
        for row in reader:
            try:
                for key in row:
                    if row[key] is None or row[key] == "":
                        row[key] = math.nan  # type: ignore
                    elif "time" in key:
                        row[key] = float(row[key])  # type: ignore
                    else:
                        row[key] = int(row[key])  # type: ignore
            except ValueError:
                print(f"Ignoring row: {row}")
                continue
            try:
                del row["seed"]
            except KeyError:
                pass
            yield row

    return read(), headers


def matches_filters(row: Dict, filters: Dict[str, int]) -> bool:
    for key in filters:
        try:
            if row[key] != filters[key]:
                return False
        except KeyError:
            return False
    return True
