import math
from itertools import groupby
from statistics import mean
from typing import Tuple, Dict, TextIO, Iterable, List, Union, IO, Optional, TypeVar

import matplotlib.pyplot as plt
import numpy as np

from qosbench.plotting.plotutil import read_and_filter

plt.rcParams["figure.figsize"] = [5, 5]


def plot(input_file: TextIO, filters: Dict[str, int], x_column: str, y_column: Optional[str] = None,
         add_hint: bool = True, output_file: Optional[IO] = None, time_columns: Optional[Iterable[str]] = None) -> None:
    data, header = read_and_filter(input_file, filters, x_column)

    data = list(data)
    time_cols = time_columns if time_columns else [h for h in header if "time" in h]
    hint_text = _get_hint_text(data, [h for h in header if h not in time_cols and h != x_column and h != y_column]) \
        if add_hint else None
    if y_column:
        plot_3d(data, time_cols, x_column, y_column, hint_text=hint_text)
    else:
        plot_areas(data, time_cols, x_column, hint_text=hint_text)

    if output_file:
        plt.savefig(output_file, transparent=True)
    else:
        plt.show()


def plot_areas(data: List[Dict], time_columns: Iterable[str], x_column: str, hint_text: Optional[str] = None) \
        -> None:
    fig = plt.figure()
    ax = plt.axes()

    # plot data
    by_x = {x: list(rows) for x, rows in groupby(data, lambda row: row[x_column])}
    grouped_data = _group_data_by_time_column(by_x, time_columns)
    for time_col, time_data in grouped_data.items():
        xs = [x for x in time_data.keys()]
        averages = [mean(time_data[x]) for x in xs]
        mins = [min(time_data[x]) for x in xs]
        maxs = [max(time_data[x]) for x in xs]
        ax.plot(xs, averages, label=time_col.replace("time", "").strip())
        ax.fill_between(xs, mins, maxs, interpolate=True, alpha=0.25)
    plt.setp(ax, ylabel="Execution time / s", xlabel=x_column)

    # configure axes
    ax.set_xbound(lower=0)
    ax.set_ybound(lower=0)
    for spine in (ax.spines.left, ax.spines.bottom):
        spine.set_position("zero")

    # hint and legend
    if hint_text:
        fig.text(0.01, 0.01, hint_text, fontsize=8)
    ax.legend()
    plt.tight_layout()


def plot_3d(data: List[Dict], time_colums: Iterable[str], x_column: str, y_column: str,
            hint_text: Optional[str] = None) -> None:
    fig = plt.figure()
    ax = plt.axes(projection="3d")
    plt.subplots_adjust(left=0, bottom=0, right=0.9, top=1, wspace=0, hspace=0)

    # plot data
    by_x_and_y = {(x, y): list(rows) for (x, y), rows in groupby(data, lambda row: (row[x_column], row[y_column]))}
    for time_col, time_data in _group_data_by_time_column(by_x_and_y, time_colums).items():
        xs, ys = map(lambda s: np.array(list(s), ndmin=2), map(set, zip(*time_data.keys())))
        ys = ys.T
        averages = np.vectorize(lambda x, y: mean(time_data[x, y]) if time_data.get((x, y)) else math.nan)(xs, ys)
        label = time_col.replace("time", "").strip()
        surf = ax.plot_surface(xs, ys, averages, label=label, alpha=0.85, )
        # fix legend, see https://github.com/matplotlib/matplotlib/issues/4067#issuecomment-753666340
        surf._facecolors2d = surf._facecolor3d
        surf._edgecolors2d = surf._edgecolor3d
    ax.set_xlabel(x_column)
    ax.set_ylabel(y_column)
    ax.set_zlabel("Execution time / s")

    # configure axes
    ax.set_zbound(lower=0)

    # hint and legend
    if hint_text:
        fig.text(0.01, 0.01, hint_text, fontsize=8)
    ax.legend()


_T = TypeVar('_T')


def _group_data_by_time_column(data: Dict[_T, List[Dict]], time_cols: Iterable[str]) -> \
        Dict[str, Dict[_T, List[float]]]:
    return {
        time_col: {
            key: [row[time_col] for row in rows]
            for key, rows in data.items()
            if not any(not math.isfinite(row[time_col]) for row in rows)
        }
        for time_col in time_cols
    }


def _get_hint_text(data: Iterable[Dict], relevant_columns: Iterable[str]) -> str:
    aggregation: Dict[str, Union[Tuple[bool, int]]] = dict()
    for row in data:
        for col in relevant_columns:
            try:
                value = row[col]
            except KeyError:
                continue
            try:
                has_single_value, single_value = aggregation[col]
                if has_single_value and value != single_value:
                    aggregation[col] = (False, value)
            except KeyError:
                aggregation[col] = (True, value)
    return "\n".join(f"{key}: {aggregation[key][1] if aggregation[key][0] else 'multiple values'}" for key in
                     aggregation)
