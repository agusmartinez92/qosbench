import math
import subprocess
import time
from typing import Optional, Iterable


class QosAggExecutor:
    def __init__(self, qosagg_cmd: Iterable[str] = ("qosagg",), timeout: Optional[float] = None):
        self.qosagg_cmd = qosagg_cmd
        self.timeout = timeout

    def execute(self, in_file: str, out_file: str) -> float:
        start = time.perf_counter()
        try:
            subprocess.check_call(args=[*self.qosagg_cmd, in_file, out_file], stdout=subprocess.DEVNULL,
                                  timeout=self.timeout)
        except subprocess.TimeoutExpired:
            return math.inf
        return time.perf_counter() - start
