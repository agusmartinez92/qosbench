import textwrap
from typing import IO, NamedTuple, Iterable


class MiniBrassGeneratorConfig(NamedTuple):
    attributes: int
    task_instances: int
    provisions: int
    attributes_max_value: int = 100


class MiniBrassGenerator:
    def __init__(self, config: MiniBrassGeneratorConfig, output: IO[str]) -> None:
        self.output = output
        self.config = config

    def generate(self) -> None:
        pvses = "".join([f"\t{pvs}\n" for pvs in self._maximization_pvses])
        pvses += f"\t{self._disjunction_pvs}\n"
        self.output.write(textwrap.dedent(f"""\
            include "defs.mbr";
            
            """))  # noqa: W293
        self.output.write(pvses)
        self.output.write(self._solve_statement)
        self.output.flush()

    @property
    def _maximization_pvses(self) -> Iterable[str]:
        for i, attribute in enumerate(self._attributes):
            yield f"""
            PVS: max{i} = new MaxCostFunctionNetwork("max{i}") {{
            \tsoft-constraint sc{i}: '{attribute}';
            }};
            """

    @property
    def _disjunction_pvs(self) -> str:
        conds = [f"{attribute} >= {self.config.task_instances * self.config.attributes_max_value/2}" for i, attribute in enumerate(self._attributes)]
        soft_constraint = " \/ ".join(conds)
        return f"""
        PVS: conds = new WeightedCsp("conds") {{
        \tsoft-constraint sc: '{soft_constraint}';
        }};
        """

    @property
    def _solve_statement(self) -> str:
        pvses = [f"max{i}" for i, _ in enumerate(self._attributes)] + ["conds"]
        pvses_combination = " lex ".join(pvses)
        return f"solve {pvses_combination};"
            
    @property
    def _attributes(self) -> Iterable[str]:
        for i in range(self.config.attributes):
            yield f"wf_aggregated_attr_{i}"
